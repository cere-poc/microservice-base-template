// package: google.rpc
// file: operation-outcome.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";
import * as issue_pb from "./issue_pb";

export class OperationOutcome extends jspb.Message { 
    clearIssueList(): void;
    getIssueList(): Array<issue_pb.Issue>;
    setIssueList(value: Array<issue_pb.Issue>): OperationOutcome;
    addIssue(value?: issue_pb.Issue, index?: number): issue_pb.Issue;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): OperationOutcome.AsObject;
    static toObject(includeInstance: boolean, msg: OperationOutcome): OperationOutcome.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: OperationOutcome, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): OperationOutcome;
    static deserializeBinaryFromReader(message: OperationOutcome, reader: jspb.BinaryReader): OperationOutcome;
}

export namespace OperationOutcome {
    export type AsObject = {
        issueList: Array<issue_pb.Issue.AsObject>,
    }
}
