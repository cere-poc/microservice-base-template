// source: severity.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.google.rpc.Severity', null, global);
/**
 * @enum {number}
 */
proto.google.rpc.Severity = {
  FATAL: 0,
  ERROR: 1,
  WARNING: 2,
  INFORMATION: 3
};

goog.object.extend(exports, proto.google.rpc);
