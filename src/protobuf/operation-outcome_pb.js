// source: operation-outcome.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var issue_pb = require('./issue_pb.js');
goog.object.extend(proto, issue_pb);
goog.exportSymbol('proto.google.rpc.OperationOutcome', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.google.rpc.OperationOutcome = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.google.rpc.OperationOutcome.repeatedFields_, null);
};
goog.inherits(proto.google.rpc.OperationOutcome, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.google.rpc.OperationOutcome.displayName = 'proto.google.rpc.OperationOutcome';
}

/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.google.rpc.OperationOutcome.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.google.rpc.OperationOutcome.prototype.toObject = function(opt_includeInstance) {
  return proto.google.rpc.OperationOutcome.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.google.rpc.OperationOutcome} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.google.rpc.OperationOutcome.toObject = function(includeInstance, msg) {
  var f, obj = {
    issueList: jspb.Message.toObjectList(msg.getIssueList(),
    issue_pb.Issue.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.google.rpc.OperationOutcome}
 */
proto.google.rpc.OperationOutcome.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.google.rpc.OperationOutcome;
  return proto.google.rpc.OperationOutcome.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.google.rpc.OperationOutcome} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.google.rpc.OperationOutcome}
 */
proto.google.rpc.OperationOutcome.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new issue_pb.Issue;
      reader.readMessage(value,issue_pb.Issue.deserializeBinaryFromReader);
      msg.addIssue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.google.rpc.OperationOutcome.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.google.rpc.OperationOutcome.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.google.rpc.OperationOutcome} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.google.rpc.OperationOutcome.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getIssueList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      issue_pb.Issue.serializeBinaryToWriter
    );
  }
};


/**
 * repeated Issue issue = 1;
 * @return {!Array<!proto.google.rpc.Issue>}
 */
proto.google.rpc.OperationOutcome.prototype.getIssueList = function() {
  return /** @type{!Array<!proto.google.rpc.Issue>} */ (
    jspb.Message.getRepeatedWrapperField(this, issue_pb.Issue, 1));
};


/**
 * @param {!Array<!proto.google.rpc.Issue>} value
 * @return {!proto.google.rpc.OperationOutcome} returns this
*/
proto.google.rpc.OperationOutcome.prototype.setIssueList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.google.rpc.Issue=} opt_value
 * @param {number=} opt_index
 * @return {!proto.google.rpc.Issue}
 */
proto.google.rpc.OperationOutcome.prototype.addIssue = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.google.rpc.Issue, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.google.rpc.OperationOutcome} returns this
 */
proto.google.rpc.OperationOutcome.prototype.clearIssueList = function() {
  return this.setIssueList([]);
};


goog.object.extend(exports, proto.google.rpc);
