// package: google.rpc
// file: severity.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export enum Severity {
    FATAL = 0,
    ERROR = 1,
    WARNING = 2,
    INFORMATION = 3,
}
