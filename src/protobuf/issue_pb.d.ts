// package: google.rpc
// file: issue.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";
import * as severity_pb from "./severity_pb";

export class Issue extends jspb.Message { 
    getSeverity(): severity_pb.Severity;
    setSeverity(value: severity_pb.Severity): Issue;
    getCode(): string;
    setCode(value: string): Issue;
    getDetails(): string;
    setDetails(value: string): Issue;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Issue.AsObject;
    static toObject(includeInstance: boolean, msg: Issue): Issue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Issue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Issue;
    static deserializeBinaryFromReader(message: Issue, reader: jspb.BinaryReader): Issue;
}

export namespace Issue {
    export type AsObject = {
        severity: severity_pb.Severity,
        code: string,
        details: string,
    }
}
