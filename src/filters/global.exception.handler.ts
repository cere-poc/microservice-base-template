import {
  ArgumentsHost,
  Catch,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { BusinessException } from '../exception/business.exception';
import { ResourceContent } from '../exception/resource.content';
import { Issue } from '../exception/issue';
import { I18nService } from 'nestjs-i18n';
import { Severity } from '../exception/severity';
import { BaseExceptionFilter } from '@nestjs/core';
import { ValidationService } from '../shared/services/validation.service';
import { COMMON_CODES, COMMON_MESSAGES } from '../common/constants/constants';
import { WrapperLoggerFactory } from '../logging/wrapper.logger.factory';

@Catch()
export class GlobalExceptionHandler extends BaseExceptionFilter {
  constructor(private readonly i18nService: I18nService) {
    super();
  }

  async catch(e: Error, host: ArgumentsHost): Promise<any> {
    const parse = ValidationService.parse(e.stack);
    const className = parse[0].methodName.split('.')[0];
    const logger = WrapperLoggerFactory.getInstance(className);

    logger.error('%o', e);

    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const resourceContent = new ResourceContent();
    let issues = new Array<Issue>();
    if (e instanceof BusinessException) {
      const rc = e.getResourceContent();
      issues = issues.concat(rc.issues);
      resourceContent.issues = issues;
      const status = rc.status || HttpStatus.BAD_REQUEST;
      logger.log(
        '%s',
        JSON.stringify(
          {
            status: status,
            response: resourceContent,
          },
          null,
          2,
        ),
      );
      response.status(status).json(resourceContent);
    } else if (e instanceof HttpException) {
      response.status(e.getStatus()).json(e.getResponse());
    } else {
      const code = this.i18nService.translate(
        await COMMON_CODES.concat('ERR_500'),
        {
          lang: 'en',
          args: null,
        },
      );
      const msg = this.i18nService.translate(
        await COMMON_MESSAGES.concat('ERR_500'),
        {
          lang: 'en',
          args: null,
        },
      );
      const defaultIssue = new Issue();
      defaultIssue.severity = Severity.ERROR;
      defaultIssue.code = await code;
      defaultIssue.details = await msg;
      issues.push(defaultIssue);
      resourceContent.issues = issues;
      logger.log(
        '%s',
        JSON.stringify(
          {
            status: HttpStatus.INTERNAL_SERVER_ERROR,
            response: resourceContent,
          },
          null,
          2,
        ),
      );
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(resourceContent);
    }
  }

  getLogger(className: string): Logger {
    return new Logger(className);
  }
}
