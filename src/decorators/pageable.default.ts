import { Direction } from 'src/common/sort/sort';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { SortGenerator } from '../shared/services/sort.generator';
import { PageRequest } from '../common/dto/page.request';
import validator from 'validator';
import isNumeric = validator.isNumeric;

export interface Pageable {
  page?: number | 1;
  size?: number | 10;
  sort?: string;
  direction?: Direction | Direction.ASC;
}

export interface Sort {
  property?: string;
  direction?: Direction | Direction.ASC;
}

export interface Sorts {
  sortDefault: Sort[];
}

export const PageableDefault = createParamDecorator(
  (data: any, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const query = request.query;
    const pageable = new PageRequest();
    if (query.page && isNumeric(query.page)) {
      pageable.page = query.page;
    } else {
      pageable.page = data.pageableDefault.page;
    }
    if (query.size && isNumeric(query.size)) {
      pageable.size = query.size;
    } else {
      pageable.size = data.pageableDefault.size;
    }
    return pageable;
  },
);

export const SortDefaults = createParamDecorator(
  (data: any, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const query = request.query;
    let sorts = [];
    if (!query.sort) {
      const sortDefaults = data.sortDefault;
      for (const sortDefault of sortDefaults) {
        sorts.push(sortDefault.sort.concat(',').concat(sortDefault.direction));
      }
    } else if (Array.isArray(query.sort)) {
      sorts = query.sort;
    } else {
      sorts.push(query.sort);
    }
    return SortGenerator.parseParameterIntoSort(sorts, ',');
  },
);
