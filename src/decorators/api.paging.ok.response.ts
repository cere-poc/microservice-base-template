import type { Type } from '@nestjs/common';

import { applyDecorators } from '@nestjs/common';
import { ApiExtraModels, ApiOkResponse, getSchemaPath } from '@nestjs/swagger';
import { PagingRs } from '../common/paging/paging.rs';

export function ApiPageOkResponse<T extends Type>(
  model: T,
  description?: string,
): MethodDecorator {
  return applyDecorators(
    ApiExtraModels(PagingRs),
    ApiOkResponse({
      description,
      schema: {
        title: `PagingRsOf${model.name}`,
        allOf: [
          { $ref: getSchemaPath(PagingRs) },
          {
            properties: {
              data: {
                type: 'array',
                items: { $ref: getSchemaPath(model) },
              },
            },
          },
        ],
      },
    }),
  );
}
