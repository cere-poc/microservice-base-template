import { Controller, Get, HttpStatus } from '@nestjs/common';
import { AppService } from './app.service';
import { BusinessException } from './exception/business.exception';
import { CommonUtils } from './shared/services/common.utils';
import { I18nService } from 'nestjs-i18n';
import { CommonMessages } from './common/constants/common.messages';
import { Severity } from './exception/severity';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly i18nService: I18nService,
  ) {}

  @Get()
  async getHello(): Promise<string> {
    throw new BusinessException(
      await CommonUtils.populateResourceContent(
        this.i18nService,
        CommonMessages.SERVICE_AUTH_REQUIRED,
        Severity.ERROR,
        HttpStatus.UNAUTHORIZED,
      ),
    );
  }
}
