import type { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

const version = require('../package.json');

export function setupSwagger(app: INestApplication): void {
  const options = new DocumentBuilder()
    .setTitle('Survey Service')
    .addBearerAuth()
    .setDescription('This is The Survey Service API Documentation')
    .setVersion(version.version)
    .addTag('Health', 'Survey Service Health check')
    .addTag('users', 'Users')
    .addTag('Stripe', 'Stripe Integration')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);
}
