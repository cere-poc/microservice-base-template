import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { UserNonPii } from '../../modules/users/user.non.pii';
import Stripe from 'stripe';
import { SubscriptionPayment } from '../../modules/payment/webhook/subscription.payment';

@Injectable()
export class ApplicationConfiguration {
  constructor(private configService: ConfigService) {}
  get isDevelopment(): boolean {
    return this.nodeEnv === 'dev';
  }

  get isProduction(): boolean {
    return this.nodeEnv === 'production';
  }

  public getNumber(key: string, defaultValue?: number): number {
    const value = this.configService.get<number>(key, defaultValue);
    if (value === undefined) {
      throw new Error(key + ' env var not set'); // probably we should call process.exit() too to avoid locking the service
    }
    try {
      return Number(value);
    } catch {
      throw new Error(key + ' env var is not a number');
    }
  }

  public getBoolean(key: string, defaultValue?: boolean): boolean {
    const value = this.configService.get<string>(key, defaultValue?.toString());
    if (value === undefined) {
      throw new Error(key + ' env var not set');
    }
    try {
      return Boolean(JSON.parse(value));
    } catch {
      throw new Error(key + ' env var is not a boolean');
    }
  }

  public getString(key: string, defaultValue?: string): string {
    const value = this.configService.get<string>(key, defaultValue);

    if (!value) {
      console.warn(`"${key}" environment variable is not set`);
      return;
    }
    return value.toString().replace(/\\n/g, '\n');
  }

  public getArrayString(key: string, determine: string): string[] {
    const value = this.configService.get<string>(key);
    if (!value) {
      console.warn(`"${key}" environment variable is not set`);
      return;
    }
    return value.split(determine);
  }

  get nodeEnv(): string {
    return this.getString('NODE_ENV', 'dev');
  }

  get fallbackLanguage(): string {
    return this.getString('FALLBACK_LANGUAGE', 'en').toLowerCase();
  }

  getRedisHost(): string {
    return this.getString('REDIS_HOST')?.toLowerCase();
  }

  getRedisPort(): number {
    return this.getNumber('REDIS_PORT', 6379);
  }

  getRedisUrl(): string {
    return 'redis://'
      .concat(this.getRedisHost())
      .concat(':')
      .concat(this.getRedisPort().toString());
  }

  get typeOrmConfig(): TypeOrmModuleOptions {
    const entities = [UserNonPii, SubscriptionPayment];
    const migrations = [__dirname + '/../../migrations/*{.ts,.js}'];
    return {
      entities,
      migrations,
      keepConnectionAlive: true,
      type: 'postgres',
      host: this.getString('DB_HOST'),
      port: this.getNumber('DB_PORT'),
      username: this.getString('DB_USERNAME'),
      password: this.getString('DB_PASSWORD'),
      database: this.getString('DB_DATABASE'),
      //subscribers: [UserSubscriber],
      migrationsRun: true,
      synchronize: true,
      logger: 'advanced-console',
      logging: this.getBoolean('ENABLE_ORMLOGS', this.isDevelopment),
    };
  }

  get stripe(): Stripe {
    return new Stripe(this.configService.get<string>('STRIPE_SECRET_KEY'), {
      apiVersion: '2020-08-27',
    });
  }

  get documentationEnabled(): boolean {
    return this.getBoolean('ENABLE_DOCUMENTATION', this.isDevelopment);
  }

  get appConfig() {
    return {
      port: this.getString('PORT'),
    };
  }
}
