import * as google_protobuf_any_pb from 'google-protobuf/google/protobuf/any_pb';
import { ResourceContent } from '../../exception/resource.content';
import { Issue } from '../../protobuf/issue_pb';
import { Severity } from '../../protobuf/severity_pb';
import { Severity as SeverityHttp } from '../../exception/severity';
import { OperationOutcome } from '../../protobuf/operation-outcome_pb';
import { Status } from '../../protobuf/status_pb';
import { Metadata, status as GrpcStatus } from '@grpc/grpc-js';
import {
  BadRequest,
  DebugInfo,
  ErrorInfo,
  Help,
  LocalizedMessage,
  PreconditionFailure,
  QuotaFailure,
  RequestInfo,
  ResourceInfo,
  RetryInfo,
} from 'src/protobuf/error_details_pb';

export const googleDeserializeMap = {
  'google.rpc.RetryInfo': RetryInfo.deserializeBinary,
  'google.rpc.DebugInfo': DebugInfo.deserializeBinary,
  'google.rpc.QuotaFailure': QuotaFailure.deserializeBinary,
  'google.rpc.PreconditionFailure': PreconditionFailure.deserializeBinary,
  'google.rpc.BadRequest': BadRequest.deserializeBinary,
  'google.rpc.RequestInfo': RequestInfo.deserializeBinary,
  'google.rpc.ResourceInfo': ResourceInfo.deserializeBinary,
  'google.rpc.Help': Help.deserializeBinary,
  'google.rpc.LocalizedMessage': LocalizedMessage.deserializeBinary,
  'google.rpc.ErrorInfo': ErrorInfo.deserializeBinary,
  'google.rpc.Status': Status.deserializeBinary,
  'google.rpc.OperationOutcome': OperationOutcome.deserializeBinary,
  'google.rpc.Issue': Issue.deserializeBinary,
};
export class StatusDetailsSerialization {
  public static serialize(
    resourceContent: ResourceContent,
    descriptorForType: string,
    grpcStatus: GrpcStatus,
  ): Status {
    const issues = resourceContent.issues.map((value) => {
      return new Issue()
        .setCode(value.code)
        .setSeverity(this.convertSeverity(value.severity))
        .setDetails(value.details);
    });
    const msg = resourceContent.issues.find((value, index) => {
      return index == 0;
    }).details;
    const operationOutcome = new OperationOutcome().setIssueList(issues);
    const anyValue = new google_protobuf_any_pb.Any();
    anyValue.pack(operationOutcome.serializeBinary(), descriptorForType);
    return new Status()
      .setCode(grpcStatus)
      .setMessage(msg)
      .setDetailsList([anyValue]);
  }

  public static defaultDeserialize(metadata: Metadata) {
    return StatusDetailsSerialization.deserialize(
      metadata,
      googleDeserializeMap,
    );
  }

  public static deserialize<
    TMap extends Record<string, (bytes: Uint8Array) => any>,
  >(
    metadata: Metadata,
    deserializeMap: TMap,
  ): {
    status: Status;
    details: Array<ReturnType<TMap[keyof TMap]>>;
  } | null {
    if (!metadata) {
      return null;
    }
    const buffer = metadata.get('grpc-status-details-bin')[0];
    if (!buffer || typeof buffer === 'string') {
      return null;
    }

    const status = Status.deserializeBinary(buffer);
    const details: Array<ReturnType<TMap[keyof TMap]>> = status
      .getDetailsList()
      .map((detail) => {
        const deserialize = deserializeMap[detail.getTypeName()];
        if (deserialize) {
          return detail.unpack(deserialize, detail.getTypeName());
        }
        return null;
      })
      .filter(notEmpty);

    return {
      status,
      details,
    };
  }

  private static convertSeverity(severity: SeverityHttp) {
    switch (severity) {
      case SeverityHttp.ERROR:
        return Severity.ERROR;
      case SeverityHttp.FATAL:
        return Severity.FATAL;
      case SeverityHttp.INFORMATION:
        return Severity.INFORMATION;
      case SeverityHttp.WARNING:
        return Severity.WARNING;
      default:
        return null;
    }
  }
}
const notEmpty = <TValue>(
  value: TValue | null | undefined,
): value is TValue => {
  return value !== null && value !== undefined;
};
