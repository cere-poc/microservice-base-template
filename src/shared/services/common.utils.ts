import { ResourceContent } from '../../exception/resource.content';
import { I18nService } from 'nestjs-i18n';
import { Issue } from '../../exception/issue';
import { Severity } from '../../exception/severity';
import {
  COMMON_CODES,
  COMMON_MESSAGES,
} from '../../common/constants/constants';
import { HttpStatus } from '@nestjs/common';
import { BusinessException } from '../../exception/business.exception';
import { errorCodeToGrpc } from '../../common/constants/http.grpc.mapper';
import { StatusDetailsSerialization } from './status.details.serialization';
import { RpcException } from '@nestjs/microservices';
import { Metadata } from '@grpc/grpc-js';

export class CommonUtils {
  private constructor() {
    //private constructor
  }
  public static async populateResourceContent(
    i18nService: I18nService,
    messageKey: string,
    severity: Severity,
    httpStatus?: HttpStatus,
    params?: any,
  ): Promise<ResourceContent> {
    const issue = new Issue();
    const resourceContent = new ResourceContent();
    resourceContent.status = httpStatus;
    issue.severity = severity;
    const code = i18nService.translate(COMMON_CODES.concat(messageKey), {
      lang: 'en',
      args: null,
    });
    const msg = i18nService.translate(COMMON_MESSAGES.concat(messageKey), {
      lang: 'en',
      args: params,
    });
    [issue.code, issue.details] = await Promise.all([code, msg]);
    resourceContent.issues.push(issue);
    return resourceContent;
  }

  public static async populateIssue(
    i18nService: I18nService,
    messageKey: string,
    severity: Severity,
    args: any,
  ) {
    const issue = new Issue();
    issue.severity = severity;
    const code = i18nService.translate(COMMON_CODES.concat(messageKey), {
      lang: 'en',
      args: null,
    });
    const msg = i18nService.translate(COMMON_MESSAGES.concat(messageKey), {
      lang: 'en',
      args: args,
    });
    [issue.code, issue.details] = await Promise.all([code, msg]);
    return issue;
  }

  public static convertBusinessExceptionToRpcException(
    e: BusinessException,
  ): RpcException {
    const status = e.status;
    const resourceContent = e.resourceContent;
    const grpcStatus = errorCodeToGrpc(status);
    const result = StatusDetailsSerialization.serialize(
      resourceContent,
      'google.rpc.OperationOutcome',
      grpcStatus,
    );
    const metaData = new Metadata();
    metaData.add(
      'grpc-status-details-bin',
      Buffer.from(result.serializeBinary()),
    );
    return new RpcException({
      code: grpcStatus,
      message: result.getMessage(),
      metadata: metaData,
    });
  }
}
