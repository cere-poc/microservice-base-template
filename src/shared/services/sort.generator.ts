import { Injectable } from '@nestjs/common';
import { Direction, Order, Sort } from '../../common/sort/sort';

const validator = require('validator');

@Injectable()
export class SortGenerator {
  public static parseParameterIntoSort(
    source: string[],
    delimiter: string,
  ): Sort {
    const allOrders = new Array<Order>();

    for (const part of source) {
      if (!part) {
        continue;
      }

      const elements = part.split(delimiter);
      const direction: Direction =
        elements.length == 0
          ? null
          : this.getDirection(elements[elements.length - 1].toUpperCase());

      for (let i = 0; i < elements.length; i++) {
        if (i == elements.length - 1 && direction != null) {
          continue;
        }

        const property = elements[i];

        if (validator.isEmpty(property)) {
          continue;
        }

        allOrders.push(new Order(direction, property));
      }
    }

    return allOrders.length == 0 ? null : new Sort(allOrders);
  }

  static getDirection(value: string): Direction {
    return Direction[value];
  }
}
