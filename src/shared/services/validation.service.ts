import { Sort } from '../../common/sort/sort';

export class ValidationService {
  public static checkPaging(fileds: string[], sorts: Sort): void {}

  public static parse(stackString) {
    const lines = stackString.split('\n');

    return lines.reduce((stack, line) => {
      const parseResult = ValidationService.parseNode(line);

      if (parseResult) {
        stack.push(parseResult);
      }

      return stack;
    }, []);
  }

  private static parseNode(line) {
    const nodeRe =
      /^\s*at (?:((?:\[object object])?[^\\/]+(?: \[as \S+])?) )?\(?(.*?):(\d+)(?::(\d+))?\)?\s*$/i;
    const UNKNOWN_FUNCTION = '<unknown>';
    const parts = nodeRe.exec(line);

    if (!parts) {
      return null;
    }

    return {
      file: parts[2],
      methodName: parts[1] || UNKNOWN_FUNCTION,
      arguments: [],
      lineNumber: +parts[3],
      column: parts[4] ? +parts[4] : null,
    };
  }
}
