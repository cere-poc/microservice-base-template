import { Global, HttpModule, Module } from '@nestjs/common';
import { ApplicationConfiguration } from './services/application.configuration';

const providers = [ApplicationConfiguration];

@Global()
@Module({
  providers,
  imports: [HttpModule],
  exports: [...providers, HttpModule],
})
export class SharedModule {}
