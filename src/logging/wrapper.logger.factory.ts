import { LoggerService } from '@nestjs/common';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';

export class WrapperLoggerFactory {
  private static buildInternalLogger(className: string): LoggerService {
    return WinstonModule.createLogger({
      defaultMeta: {
        service: 'example-service',
      },
      level: 'debug',
      transports: [
        new winston.transports.Console({
          level: 'debug',
          format: winston.format.combine(
            winston.format.splat(),
            winston.format.label({
              label: className,
              message: true,
            }),
            winston.format.json(),
            winston.format.timestamp(),
            winston.format.colorize({
              all: true,
            }),
            winston.format.printf((log) => {
              return `[ExampleApplication] ${process.pid} - ${log.timestamp}      ${log.level} [${className}] ${log.message}`;
            }),
          ),
        }),
      ],
    });
  }
  public static getInstance(className: string): LoggerService {
    return this.buildInternalLogger(className);
  }
}
