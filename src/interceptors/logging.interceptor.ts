import {
  CallHandler,
  ExecutionContext,
  Injectable,
  Logger,
  NestInterceptor,
} from '@nestjs/common';
import { catchError, Observable, throwError } from 'rxjs';
import { tap } from 'rxjs/operators';
import { WrapperLoggerFactory } from '../logging/wrapper.logger.factory';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Observable<any> | Promise<Observable<any>> {
    const logger = WrapperLoggerFactory.getInstance(context.getClass().name);
    const { originalUrl, method, params, query, body } = context
      .switchToHttp()
      .getRequest();
    const { statusCode } = context.switchToHttp().getResponse();
    logger.log('%o', { originalUrl, method, params, query, body });
    return next.handle().pipe(
      tap((data) => {
        logger.log('%o', {
          statusCode,
          data,
        });
      }),
    );
  }
}
