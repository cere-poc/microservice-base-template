import { Converter } from '@automapper/types';
import * as moments from 'moment';

export const toLocalDateStringConverter: Converter<Date, string> = {
  convert(source: Date): string {
    if (!source) {
      return null;
    }
    return moments(source).format('yyyy-MM-DD');
  },
};

export const stringToLocalDateConverter: Converter<string, Date> = {
  convert(source: string): Date {
    return moments(source, 'yyyy-MM-DD').toDate();
  },
};
