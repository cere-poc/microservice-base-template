import { NestApplication, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  ExpressAdapter,
  NestExpressApplication,
} from '@nestjs/platform-express';
import { setupSwagger } from './setup-swagger';
import { SharedModule } from './shared/shared.module';
import { ApplicationConfiguration } from './shared/services/application.configuration';

import * as compression from 'compression';
import * as helmet from 'helmet';
import * as cookieParser from 'cookie-parser';
import * as csurf from 'csurf';
import { ClassSerializerInterceptor, HttpStatus, Logger } from '@nestjs/common';
import { MicroserviceOptions } from '@nestjs/microservices';
import { microserviceOptions } from './modules/client/grcp.options';
import * as winston from 'winston';
import { WinstonModule } from 'nest-winston';

declare const module: any;

async function bootstrap(): Promise<NestExpressApplication> {
  const logger = new Logger(NestApplication.name);
  const app = await NestFactory.create<NestExpressApplication>(
    AppModule,
    new ExpressAdapter(),
    {
      logger: WinstonModule.createLogger({
        transports: [
          new winston.transports.Console({
            format: winston.format.combine(
              winston.format.splat(),
              winston.format.timestamp(),
              winston.format.colorize({
                all: true,
              }),
              winston.format.printf((log) => {
                return `[ExampleApplication] ${process.pid} - ${log.timestamp}      ${log.level} [${log.context}] ${log.message}`;
              }),
            ),
          }),
        ],
      }),
    },
  );

  app.enableCors({
    origin: '*',
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    allowedHeaders: '*',
    credentials: true,
    exposedHeaders: '*',
    maxAge: 3600,
    optionsSuccessStatus: HttpStatus.OK,
  });

  app.use(compression());
  app.use(helmet());
  // app.use(cookieParser());
  // app.use(
  //   csurf({
  //     cookie: false,
  //   }),
  // );

  const configService = app.select(SharedModule).get(ApplicationConfiguration);
  if (configService.documentationEnabled) {
    setupSwagger(app);
  }
  app.enableShutdownHooks();
  app.connectMicroservice<MicroserviceOptions>(microserviceOptions);
  await app.startAllMicroservices();
  const port = configService.appConfig.port;
  await app.listen(port, '0.0.0.0', () => {
    logger.log(`server running on port ${port}`);
  });

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
  return app;
}
void bootstrap();
