import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { CommonMessages } from '../common/constants/common.messages';

@ValidatorConstraint({
  async: true,
})
export class EmailValidatorConstraint implements ValidatorConstraintInterface {
  defaultMessage(validationArguments?: ValidationArguments): any {
    if (!validationArguments.value) {
      return CommonMessages.REQUIRED;
    }
    return CommonMessages.EMAIL;
  }

  validate(
    value: any,
    validationArguments?: ValidationArguments,
  ): Promise<boolean> | boolean {
    return false;
  }
}
