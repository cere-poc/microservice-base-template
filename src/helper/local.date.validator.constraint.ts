import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import * as moments from 'moment';
import { ConstraintConstants } from '../common/constants/constraint.constants';

@ValidatorConstraint({ name: ConstraintConstants.LOCAL_DATE, async: true })
export class LocalDateValidatorConstraint
  implements ValidatorConstraintInterface
{
  defaultMessage(validationArguments?: ValidationArguments): string {
    return '';
  }

  async validate(value: string): Promise<boolean> {
    if (!value) {
      return true;
    }
    try {
      moments(value, 'yyyy-MM-DD').toDate();
      return true;
    } catch (e) {
      return false;
    }
  }
}
