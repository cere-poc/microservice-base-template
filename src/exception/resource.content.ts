import { Issue } from './issue';
import { HttpStatus } from '@nestjs/common';
import { Exclude } from 'class-transformer';

export class ResourceContent {
  resourceType: string;
  issues: Array<Issue> = new Array<Issue>();
  @Exclude()
  status?: HttpStatus;
  @Exclude()
  message?: string;
}
