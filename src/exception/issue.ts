import { Severity } from './severity';
import { ApiProperty } from '@nestjs/swagger';

export class Issue {
  @ApiProperty({
    description:
      'This value set includes codes from the following code systems',
    enum: Severity,
  })
  severity: Severity;
  @ApiProperty({
    description: 'Error or warning code',
    example: 'SERVICE_XXX_AUTH_REQUIRED',
  })
  code: string;
  @ApiProperty({
    description: 'Additional details about the error',
    example: 'You must authenticate before you can use service {serviceName}.',
  })
  details: string;
  @ApiProperty({
    description: 'Additional diagnostic information about the issue',
  })
  diagnostics: string;
  expression: string[];

}
