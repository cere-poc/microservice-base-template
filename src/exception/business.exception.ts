import { ResourceContent } from './resource.content';
import { HttpStatus } from '@nestjs/common';

export class BusinessException extends Error {
  private readonly _resourceContent: ResourceContent;
  private readonly _status: HttpStatus;

  constructor(resourceContent: ResourceContent) {
    super();
    this._resourceContent = resourceContent;
  }

  public getResourceContent(): ResourceContent {
    return this._resourceContent;
  }

  get status(): HttpStatus {
    return this._status;
  }

  get resourceContent(): ResourceContent {
    return this._resourceContent;
  }
}
