import { Logger, OnModuleInit } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import { ApplicationConfiguration } from 'src/shared/services/application.configuration';
import { KafkaInfo, Topic } from '../constant';
import { KafkaClient } from '../kafka.client';

@Injectable()
export class UserConsumeHandler implements OnModuleInit {
  private logger = new Logger('UserConsumeHandler');

  constructor(
    private kafkaClient: KafkaClient,
    private appConfig: ApplicationConfiguration,
  ) {} //Call when dependency was resovled
  onModuleInit() {
    if (this.appConfig.getBoolean(KafkaInfo.KAFKA_ENABLE)) this.handleMessage();
  }

  async handleMessage(): Promise<void> {
    const consumer = this.kafkaClient.getConsumer('group_id');
    await consumer.connect();
    await consumer.subscribe({
      topic: Topic.createUserTopic,
      fromBeginning: true,
    });
    await consumer.run({
      eachMessage: async ({ topic, partition, message }) => {
        this.logger.log(
          'Request receive from topic ' + topic + ' and partition ' + partition,
        );
        this.logger.log({
          key: message.key.toString(),
          value: message.value.toString(),
          headers: message.headers,
        });
      },
    });
  }
}
