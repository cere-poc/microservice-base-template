import { Injectable } from '@nestjs/common';
import { Logger } from '@nestjs/common';
import { Body, Post } from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { EventName, Topic } from '../constant';
import { CreateUserEvent } from '../dto/user.dto.event';
import { KafkaClient } from '../kafka.client';

@Controller('/api/v1/producer/user')
@Injectable()
export class UserProducerController {
  private logger = new Logger('UserProducerController');

  constructor(private kafkaClient: KafkaClient) {}

  @Post('create')
  async create(@Body() createEvent: CreateUserEvent): Promise<CreateUserEvent> {
    createEvent.eventName = EventName.CREATE_USER;
    createEvent.eventTime = Date.now();
    createEvent.requestId = 'request_id_' + createEvent.eventTime;
    const producer = this.kafkaClient.getProducer();
    await producer.connect();
    await producer
      .send({
        topic: Topic.createUserTopic,
        messages: [
          {
            key: createEvent.requestId,
            value: JSON.stringify(createEvent),
          },
        ],
      })
      .catch((e) => {
        this.logger.error('Exception create user event ', e);
        throw e;
      });
    return createEvent;
  }
}
