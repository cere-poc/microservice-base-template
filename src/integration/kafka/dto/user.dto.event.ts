import { AutoMap } from '@automapper/classes';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, Validate } from 'class-validator';
import { LocalDateValidatorConstraint } from 'src/helper/local.date.validator.constraint';
import { BaseEventDTO } from '../event.base.dto';

export class CreateUserEvent extends BaseEventDTO {
  constructor() {
    super();
  }
  userId: string;

  @ApiProperty()
  @AutoMap()
  readonly firstName: string;

  @ApiProperty()
  @AutoMap()
  readonly lastName: string;

  @ApiProperty()
  @AutoMap()
  readonly middleName: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  @AutoMap()
  readonly email: string;

  @ApiProperty()
  @IsNotEmpty()
  @AutoMap()
  readonly userName: string;

  @ApiProperty()
  @AutoMap()
  mobilePhoneNumber: string;

  @ApiProperty()
  @Validate(LocalDateValidatorConstraint, {})
  dateOfBirth: string;

  @ApiProperty()
  @AutoMap()
  gender: string;
}
