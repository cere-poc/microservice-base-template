import { EventName } from './constant';

export class BaseEventDTO {
  public eventTime: number; //Thoi gian event duoc gui di
  public requestId: string; //Unique id su dung lam trace cho moi event duoc gui di
  public eventName: EventName;
}
