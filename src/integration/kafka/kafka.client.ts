import { Injectable } from '@nestjs/common';
import { Kafka, KafkaConfig } from 'kafkajs';
import { Consumer, Producer } from 'kafkajs';
import { ApplicationConfiguration } from 'src/shared/services/application.configuration';
import { KafkaInfo } from './constant';

@Injectable()
export class KafkaClient {
  private kafkaClient: Kafka;

  constructor(private applicationConfig: ApplicationConfiguration) {
    this.kafkaClient = new Kafka(this.kafkaConfig);
  }

  public getProducer(): Producer {
    return this.kafkaClient.producer();
  }

  public getConsumer(groupId: string): Consumer {
    return this.kafkaClient.consumer({ groupId: groupId });
  }

  public getClient(): Kafka {
    return this.kafkaClient;
  }

  get kafkaConfig(): KafkaConfig {
    const determine = ';';
    return {
      brokers: this.applicationConfig.getArrayString(
        KafkaInfo.KAFKA_BROKER,
        determine,
      ),
      connectionTimeout: this.applicationConfig.getNumber(
        KafkaInfo.KAFKA_CONNECTION_TIMEOUT,
      ),
      clientId: this.applicationConfig.getString(KafkaInfo.KAFKA_CLIENT_ID),
    };
  }
}
