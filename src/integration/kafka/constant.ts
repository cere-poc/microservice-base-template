export const enum EventName {
  CREATE_USER = 'CREATE_USER',
}

export const Topic = {
  createUserTopic: 'user-create.event',
};

export const KafkaInfo = {
  KAFKA_BROKER: 'KAFKA_BROKER',
  KAFKA_CONNECTION_TIMEOUT: 'KAFKA_CONNECTION_TIMEOUT',
  KAFKA_CLIENT_ID: 'KAFKA_CLIENT_ID',
  KAFKA_ENABLE: 'KAFKA_ENABLE',
};
