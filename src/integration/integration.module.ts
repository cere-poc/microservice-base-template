import { Module } from '@nestjs/common';
import { UserConsumeHandler } from './kafka/controller/user.consumer';
import { UserProducerController } from './kafka/controller/user.controller';
import { KafkaClient } from './kafka/kafka.client';

@Module({
  controllers: [UserProducerController],
  providers: [KafkaClient, UserConsumeHandler],
  exports: [KafkaClient],
})
export class IntegrationModule {}
