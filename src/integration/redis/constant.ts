export const REDIS_KEY = {
  USER: 'USER',
};

export const RedisInfo = {
  REDIS_TTL: 'REDIS_TTL',
  REDIS_HOST: 'REDIS_HOST',
  REDIS_PORT: 'REDIS_PORT',
}
