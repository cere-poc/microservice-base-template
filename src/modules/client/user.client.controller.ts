import {
  Body,
  Controller,
  Get,
  Logger,
  OnModuleInit,
  Param,
  Post,
} from '@nestjs/common';
import { microserviceOptions } from './grcp.options';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { UserService } from './user.service';
import { UserDto } from '../users/dto/user.dto';
import { catchError, map } from 'rxjs';

@Controller('/api/v1/internal')
export class UserClientController implements OnModuleInit {
  private logger = new Logger('UserClientController');

  @Client(microserviceOptions) // <-- Add
  private client: ClientGrpc; // <-- this

  private grpcService: UserService;
  onModuleInit(): any {
    this.grpcService = this.client.getService<UserService>('UserController');
  }

  @Post('create')
  async createUser(@Body() data: UserDto) {
    this.logger.log('Adding ' + data.userName);
    return this.grpcService.create(data).pipe(
      map((user) => user),
      catchError((e: any) => {
        throw e;
      }),
    );
  }

  @Get('/users/:userId')
  async getUser(@Param('userId') userId: string) {
    return this.grpcService.getUser({
      userId: userId,

    });
  }
}
