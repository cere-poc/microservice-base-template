import { Observable } from 'rxjs';
import { UserDto } from '../users/dto/user.dto';

export interface UserService {
  create(user: UserDto): Observable<any>;
  getUser(data: any): Observable<any>;
}
