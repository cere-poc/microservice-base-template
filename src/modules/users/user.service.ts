import { UserDto } from './dto/user.dto';
import { UserNonPii } from './user.non.pii';
import { Injectable } from '@nestjs/common';
import { UserRepository } from './user.repository';
import { getManager } from 'typeorm';
import { PageRequest } from '../../common/dto/page.request';
import { PagingRs } from '../../common/paging/paging.rs';
import { UserNonPiiResponseDto } from './dto/user.non.pii.response.dto';
import { Mapper } from '@automapper/types';
import { InjectMapper } from '@automapper/nestjs';
import { ResourceContent } from '../../exception/resource.content';
import { CommonUtils } from '../../shared/services/common.utils';
import { I18nService } from 'nestjs-i18n';
import { CommonMessages } from '../../common/constants/common.messages';
import { Severity } from '../../exception/severity';
import { BusinessException } from '../../exception/business.exception';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserRequest } from './dto/create.user.request';
import { ApplicationConfiguration } from '../../shared/services/application.configuration';
import { Stripe } from 'stripe';

@Injectable()
export class UserService {
  private stripe: Stripe;

  constructor(
    @InjectRepository(UserRepository)
    private userNonPiiRepository: UserRepository,
    @InjectMapper() private mapper: Mapper,
    private i18nService: I18nService,
    private applicationConfig: ApplicationConfiguration,
  ) {
    this.stripe = applicationConfig.stripe;
  }
  async createUser(
    userRequest: CreateUserRequest,
  ): Promise<UserNonPiiResponseDto> {
    const found = await this.userNonPiiRepository
      .createQueryBuilder('user')
      .andWhere('user.isDeleted = :isDeleted', { isDeleted: false })
      .andWhere('(user.userName = :userName OR user.email = :email)', {
        userName: userRequest.userName,
        email: userRequest.email,
      })
      .getMany();
    const isEmailExist =
      found.filter((value) => {
        return value.email === userRequest.email;
      }).length > 0;
    const isUserNameExist =
      found.filter((value) => {
        return value.userName === userRequest.userName;
      }).length > 0;
    if (isEmailExist || isUserNameExist) {
      const resourceContent = new ResourceContent();
      if (isEmailExist) {
        resourceContent.issues.push(
          await CommonUtils.populateIssue(
            this.i18nService,
            CommonMessages.ENTITY_EXISTS,
            Severity.ERROR,
            {
              entityName: 'email',
            },
          ),
        );
      }
      if (isUserNameExist) {
        resourceContent.issues.push(
          await CommonUtils.populateIssue(
            this.i18nService,
            CommonMessages.ENTITY_EXISTS,
            Severity.ERROR,
            {
              entityName: 'userName',
            },
          ),
        );
      }
      throw new BusinessException(resourceContent);
    }
    const userNonPii = this.mapper.map(
      userRequest,
      UserNonPii,
      CreateUserRequest,
    );
    const stripeCustomer = await this.stripe.customers.create({
      email: userNonPii.email,
      name: userNonPii.firstName.concat(',').concat(userNonPii.lastName),
    });
    userNonPii.stripeCustomerId = stripeCustomer.id;
    await getManager().transaction(async (em) => {
      await em.save(userNonPii);
    });
    return Promise.resolve(
      this.mapper.map(userNonPii, UserNonPiiResponseDto, UserNonPii),
    );
  }

  async getOne(userId: string): Promise<UserNonPiiResponseDto> {
    const userNonPii = await this.userNonPiiRepository.findOne({
      userId: userId,
      isDeleted: false,
    });
    return this.mapper.map(userNonPii, UserNonPiiResponseDto, UserNonPii);
  }

  async getUsers(
    pageable: PageRequest,
  ): Promise<PagingRs<UserNonPiiResponseDto>> {
    const pagingRs = new PagingRs<UserNonPiiResponseDto>();
    const sort = pageable.sort;
    // ValidationService.checkPaging(['userId'], sort);
    const builder = this.userNonPiiRepository
      .createQueryBuilder('user')
      .andWhere('user.isDeleted = :isDeleted', { isDeleted: false });
    for (const order of sort.orders) {
      builder.addOrderBy('user.'.concat(order.property), order.direction);
    }
    builder.offset(pageable.getOffset()).limit(pageable.getPageSize());
    const number = await builder.getCount();
    pagingRs.totalCount = number;
    if (number === 0) {
      pagingRs.data = null;
    } else {
      const userNonPiis = await builder.getMany();
      if (userNonPiis.length == 0) pagingRs.data = null;
      else {
        pagingRs.data = new Array<UserNonPiiResponseDto>();
        userNonPiis.forEach((value) => {
          pagingRs.data.push(
            this.mapper.map(value, UserNonPiiResponseDto, UserNonPii),
          );
        });
      }
    }
    return Promise.resolve(pagingRs);
  }

  async deleteUser(userId: string) {
    const userFound = await this.userNonPiiRepository.findOne(userId, {
      where: {
        isDeleted: false,
      },
    });
    if (!userFound) {
      return null;
    }
    userFound.isDeleted = true;
    await getManager().transaction(async (em) => {
      await em.save(userFound);
    });
    return userFound;
  }

  async updateUser(userId: string, userDto: UserDto) {
    userDto.userId = userId;
    const user = await this.userNonPiiRepository.findOne({
      where: {
        userId: userId,
      },
    });
    if (!user) {
      return null;
    }
    const found = await this.userNonPiiRepository
      .createQueryBuilder('user')
      .andWhere('user.isDeleted = :isDeleted', { isDeleted: false })
      .andWhere('user.userId != :userId', { userId: userId })
      .andWhere('(user.userName = :userName OR user.email = :email)', {
        userName: userDto.userName,
        email: userDto.email,
      })
      .getMany();
    const isEmailExist =
      found.filter((value) => {
        return value.email === userDto.email;
      }).length > 0;
    const isUserNameExist =
      found.filter((value) => {
        return value.userName === userDto.userName;
      }).length > 0;
    if (isEmailExist || isUserNameExist) {
      const resourceContent = new ResourceContent();
      if (isEmailExist) {
        resourceContent.issues.push(
          await CommonUtils.populateIssue(
            this.i18nService,
            CommonMessages.ENTITY_EXISTS,
            Severity.ERROR,
            {
              entityName: 'email',
            },
          ),
        );
      }
      if (isUserNameExist) {
        resourceContent.issues.push(
          await CommonUtils.populateIssue(
            this.i18nService,
            CommonMessages.ENTITY_EXISTS,
            Severity.ERROR,
            {
              entityName: 'userName',
            },
          ),
        );
      }
      throw new BusinessException(resourceContent);
    }
    const userNonPii = this.mapper.map(userDto, UserNonPii, UserDto);
    userNonPii.userId = user.userId;
    await getManager().transaction(async (em) => {
      await em.save(UserNonPii, userNonPii);
    });
    return Promise.resolve(
      this.mapper.map(userNonPii, UserNonPiiResponseDto, UserNonPii),
    );
  }
}
