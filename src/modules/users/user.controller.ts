import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { UserDto } from './dto/user.dto';
import { UserNonPii } from './user.non.pii';
import { UserService } from './user.service';
import { parse as uuidParse } from 'uuid';
import { Response } from 'express';
import { CommonResponse } from '../../common/common.response';
import { PageableDefault, SortDefaults } from 'src/decorators/pageable.default';
import { Direction, Sort } from 'src/common/sort/sort';
import { PageRequest } from '../../common/dto/page.request';
import { PagingRs } from '../../common/paging/paging.rs';
import { ApiPageOkResponse } from '../../decorators/api.paging.ok.response';
import { UserNonPiiResponseDto } from './dto/user.non.pii.response.dto';
import { CustomValidationPipe } from '../../pipes/custom.validation.pipe';
import { BusinessException } from '../../exception/business.exception';
import { CommonUtils } from '../../shared/services/common.utils';
import { I18nService } from 'nestjs-i18n';
import { CommonMessages } from '../../common/constants/common.messages';
import { Severity } from '../../exception/severity';
import { GrpcMethod, RpcException } from '@nestjs/microservices';
import { status } from '@grpc/grpc-js';
import { CreateUserRequest } from './dto/create.user.request';

@ApiTags('users')
@Controller('api/v1')
export class UserController {
  constructor(
    private userService: UserService,
    private i18nService: I18nService,
  ) {}

  @Post('/users')
  @HttpCode(HttpStatus.CREATED)
  async createUser(
    @Body(CustomValidationPipe)
    userRequest: CreateUserRequest,
  ): Promise<UserNonPiiResponseDto> {
    return this.userService.createUser(userRequest);
  }

  @Get('/users/:userId')
  @ApiOkResponse({
    type: UserNonPii,
  })
  @HttpCode(HttpStatus.OK)
  async getUserByUserId(
    @Param('userId') userId: string,
    @Res({ passthrough: true }) res: Response,
  ): Promise<UserNonPiiResponseDto> {
    if (!this.isUuidValid(userId)) {
      res.status(HttpStatus.NO_CONTENT);
      return;
    }
    const userFound = await this.userService.getOne(userId);
    if (!userFound) {
      res.status(HttpStatus.NO_CONTENT);
    } else {
      return Promise.resolve(userFound);
    }
  }

  isUuidValid(uuid: string): boolean {
    try {
      uuidParse(uuid);
      return true;
    } catch (e) {
      return false;
    }
  }

  @Delete('/users/:userId')
  @HttpCode(HttpStatus.OK)
  async deleteUser(
    @Param('userId') userId: string,
    @Res({ passthrough: true }) res: Response,
  ): Promise<CommonResponse> {
    const response = new CommonResponse();
    const userDropped = await this.userService.deleteUser(userId);
    if (userDropped) {
      response.success = true;
      return Promise.resolve(response);
    } else {
      throw new BusinessException(
        await CommonUtils.populateResourceContent(
          this.i18nService,
          CommonMessages.ENTITY_WITH_DETAIL_NOTFOUND,
          Severity.ERROR,
          HttpStatus.BAD_REQUEST,
          { field: 'userId', entityName: 'user', value: userId },
        ),
      );
    }
  }

  @Get('/users')
  @ApiPageOkResponse(UserNonPiiResponseDto, 'Return Paging of Users')
  @HttpCode(HttpStatus.OK)
  async getUsers(
    @PageableDefault({
      pageableDefault: {
        page: 1,
        size: 50,
      },
    })
    pageable: PageRequest,
    @SortDefaults({
      sortDefault: [
        {
          direction: Direction.DESC,
          sort: 'createdOn',
        },
      ],
    })
    sorts: Sort,
  ): Promise<PagingRs<UserNonPiiResponseDto>> {
    pageable.sort = sorts;
    return Promise.resolve(this.userService.getUsers(pageable));
  }

  @Put('/users/:userId')
  async updateUser(
    @Param('userId') userId: string,
    @Body(CustomValidationPipe)
    userRequest: UserDto,
    @Res({ passthrough: true }) res: Response,
  ) {
    const responseDto = await this.userService.updateUser(userId, userRequest);
    if (!responseDto) {
      res.status(HttpStatus.NO_CONTENT);
    } else {
      return Promise.resolve(responseDto);
    }
  }

  @GrpcMethod('UserController', 'create')
  createInternal(
    userRequest: CreateUserRequest,
  ): Promise<UserNonPiiResponseDto> {
    try {
      return this.userService.createUser(userRequest);
    } catch (e: any) {
      throw CommonUtils.convertBusinessExceptionToRpcException(e);
    }
  }

  @GrpcMethod('UserController', 'delete')
  async deleteInternal(userId: string): Promise<CommonResponse> {
    const response = new CommonResponse();
    const userDropped = await this.userService.deleteUser(userId);
    if (userDropped) {
      response.success = true;
      return Promise.resolve(response);
    } else {
      throw CommonUtils.convertBusinessExceptionToRpcException(
        new BusinessException(
          await CommonUtils.populateResourceContent(
            this.i18nService,
            CommonMessages.ENTITY_WITH_DETAIL_NOTFOUND,
            Severity.ERROR,
            HttpStatus.BAD_REQUEST,
            { field: 'userId', entityName: 'user', value: userId },
          ),
        ),
      );
    }
  }

  @GrpcMethod('UserController', 'getUser')
  async getUserByIdInternal(data: any) {
    return this.userService.getOne(data.userId);
  }
}
