import { UserNonPii } from './user.non.pii';
import { EntityRepository, Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';

@EntityRepository(UserNonPii)
@Injectable()
export class UserRepository extends Repository<UserNonPii> {}
