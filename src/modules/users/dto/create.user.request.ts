import { UserDto } from './user.dto';
import { ApiProperty } from '@nestjs/swagger';
import { MinLength } from 'class-validator';
import { AutoMap } from '@automapper/classes';

export class CreateUserRequest extends UserDto {
  @ApiProperty({ minLength: 6 })
  @MinLength(8, {
    context: {
      value: 8,
    },
  })
  @AutoMap()
  readonly password: string;
}
