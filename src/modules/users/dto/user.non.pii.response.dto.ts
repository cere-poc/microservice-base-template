import { ApiProperty } from '@nestjs/swagger';
import { AutoMap } from '@automapper/classes';

export class UserNonPiiResponseDto {
  @ApiProperty({
    description: 'The userId of user',
    example: '9a1f578e-1c10-493d-abed-84794ba59303',
  })
  readonly userId: string;

  @ApiProperty()
  @AutoMap()
  readonly firstName: string;

  @ApiProperty()
  @AutoMap()
  readonly lastName: string;

  @ApiProperty()
  @AutoMap()
  readonly middleName: string;

  @ApiProperty()
  @AutoMap()
  readonly email: string;

  @ApiProperty()
  @AutoMap()
  readonly userName: string;

  @ApiProperty()
  @AutoMap()
  mobilePhoneNumber: string;

  @ApiProperty()
  dateOfBirth: string;

  @ApiProperty()
  @AutoMap()
  gender: string;

  @ApiProperty()
  @AutoMap()
  stripeCustomerId: string;
}
