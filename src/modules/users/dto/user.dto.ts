import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MinLength, Validate } from 'class-validator';
import { LocalDateValidatorConstraint } from '../../../helper/local.date.validator.constraint';
import { AutoMap } from '@automapper/classes';

export class UserDto {
  userId: string;

  @ApiProperty()
  @AutoMap()
  readonly firstName: string;

  @ApiProperty()
  @AutoMap()
  readonly lastName: string;

  @ApiProperty()
  @AutoMap()
  readonly middleName: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  @AutoMap()
  readonly email: string;

  @ApiProperty()
  @IsNotEmpty()
  @AutoMap()
  readonly userName: string;

  @ApiProperty()
  @AutoMap()
  mobilePhoneNumber: string;

  @ApiProperty()
  @Validate(LocalDateValidatorConstraint, {})
  dateOfBirth: string;

  @ApiProperty()
  @AutoMap()
  gender: string;
}
