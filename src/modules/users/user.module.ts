import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { UserNonPiiResponseMapper } from './mapper/user.non.pii.response.mapper';
import { UserNonPiiRequestMapper } from './mapper/user.non.pii.request.mapper';
import { UserRequestMapper } from './mapper/user.request.mapper';

@Module({
  imports: [TypeOrmModule.forFeature([UserRepository])],
  controllers: [UserController],
  exports: [UserService, UserRepository],
  providers: [
    UserService,
    UserNonPiiResponseMapper,
    UserNonPiiRequestMapper,
    UserRequestMapper,
    UserRepository,
  ],
})
export class UserModule {}
