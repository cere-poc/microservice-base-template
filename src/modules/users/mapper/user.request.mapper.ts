import { AutomapperProfile, InjectMapper } from '@automapper/nestjs';
import { Mapper, MappingProfile } from '@automapper/types';
import { UserDto } from '../dto/user.dto';
import { UserNonPii } from '../user.non.pii';
import { mapFrom } from '@automapper/core';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserRequestMapper extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }
  mapProfile(): MappingProfile {
    return (mapper) => {
      mapper.createMap(UserDto, UserNonPii).forMember(
        (dest) => dest.userId,
        mapFrom((source) => source.userId),
      );
    };
  }
}
