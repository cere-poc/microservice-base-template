import { AutomapperProfile, InjectMapper } from '@automapper/nestjs';
import { Mapper, MappingProfile } from '@automapper/types';
import { Injectable } from '@nestjs/common';
import { UserNonPii } from '../user.non.pii';
import { convertUsing, ignore, mapFrom } from '@automapper/core';
import { stringToLocalDateConverter } from '../../../converter/converter';
import { CreateUserRequest } from '../dto/create.user.request';

@Injectable()
export class UserNonPiiRequestMapper extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }

  mapProfile(): MappingProfile {
    return (mapper) => {
      mapper
        .createMap(CreateUserRequest, UserNonPii)
        .forMember(
          (destination) => destination.dateOfBirth,
          convertUsing(
            stringToLocalDateConverter,
            (source) => source.dateOfBirth,
          ),
        )
        .forMember(
          (destination) => destination.password,
          mapFrom((source) => source.password),
        )
        .forMember((destination) => destination.userId, ignore());
    };
  }
}
