import { AutomapperProfile, InjectMapper } from '@automapper/nestjs';
import { Mapper, MappingProfile } from '@automapper/types';
import { Injectable } from '@nestjs/common';
import { UserNonPii } from '../user.non.pii';
import { convertUsing, mapFrom } from '@automapper/core';
import { toLocalDateStringConverter } from '../../../converter/converter';
import { UserNonPiiResponseDto } from '../dto/user.non.pii.response.dto';

@Injectable()
export class UserNonPiiResponseMapper extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }

  mapProfile(): MappingProfile {
    return (mapper) => {
      mapper
        .createMap(UserNonPii, UserNonPiiResponseDto)
        .forMember(
          (destination) => destination.dateOfBirth,
          convertUsing(
            toLocalDateStringConverter,
            (source) => source.dateOfBirth,
          ),
        )
        .forMember(
          (obj) => obj.userId,
          mapFrom((source) => source.userId),
        );
    };
  }
}
