import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { AbstractEntity } from '../../common/abstract.entity';
import { AutoMap } from '@automapper/classes';
import { SubscriptionPayment } from '../payment/webhook/subscription.payment';

@Entity({
  name: 'user_non_pii',
})
export class UserNonPii extends AbstractEntity {
  @Column({
    name: 'user_id',
    type: 'uuid',
    primary: true,
    generated: 'uuid',
  })
  userId: string;
  @Column({
    name: 'user_name',
    type: 'varchar',
    length: 2000,
  })
  @AutoMap()
  userName: string;
  @Column({
    name: 'password',
    type: 'varchar',
    length: 1000,
  })
  password: string;
  @Column({
    type: 'varchar',
    name: 'first_name',
  })
  @AutoMap()
  firstName: string;
  @Column({
    type: 'varchar',
    name: 'middle_name',
    nullable: true,
  })
  @AutoMap()
  middleName: string;
  @Column({
    type: 'varchar',
    name: 'last_name',
  })
  @AutoMap()
  lastName: string;
  @Column({
    name: 'email',
    type: 'varchar',
    length: 1000,
  })
  @AutoMap()
  email: string;
  @Column({
    name: 'mobile_phone_number',
    type: 'varchar',
    nullable: true,
  })
  @AutoMap()
  mobilePhoneNumber: string;
  @Column({
    name: 'gender',
    type: 'varchar',
    nullable: true,
  })
  @AutoMap()
  gender: string;
  @Column({
    name: 'date_of_birth',
    type: 'date',
    nullable: true,
  })
  dateOfBirth: Date;
  @Column({
    name: 'is_deleted',
    type: 'bool',
    default: false,
  })
  isDeleted: boolean;

  @AutoMap()
  @Column({
    name: 'customer_id',
    type: 'varchar',
    nullable: true,
  })
  stripeCustomerId: string;

  @OneToMany(
    () => SubscriptionPayment,
    (subscriptionPayment) => subscriptionPayment.userNonPii,
    {
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      lazy: true,
      nullable: true,
    },
  )
  subscriptionPayments: SubscriptionPayment[];
}
