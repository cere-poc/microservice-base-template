import { ApiProperty } from '@nestjs/swagger';

export class PaymentIntent {
  @ApiProperty()
  readonly amount: number;
  @ApiProperty()
  readonly currency: string;
  @ApiProperty()
  readonly confirm: boolean;
  @ApiProperty()
  readonly paymentMethod: string;
}
