export interface InvoiceSetting{
    custom_fields: {name,value};
    default_payment_method?:string;
    footer?:string;
}