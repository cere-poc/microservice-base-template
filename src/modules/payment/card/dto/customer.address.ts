export interface CustomerAddress {
  readonly city: string;
  readonly country: string;
  readonly line1: string;
  readonly line2: string;
  readonly postal_code: string;
  readonly state: string;
}
