export interface ShippingAddress {
  readonly address: string;
  readonly name: string;
  readonly phone: string;
}
