import { ApiProperty } from '@nestjs/swagger';

export class CardRequestDto {
  @ApiProperty({
    example: 'tok_1JIuY9JBx4qXanmKpRBf2zaq',
    description: 'The Stripe token retrieve from front end app',
  })
  source: string;
  metadata: any;
}
