import { CustomerAddress } from './customer.address';
import { ShippingAddress } from './shipping.address';
import { Tax } from './tax';
import {InvoiceSetting} from "./invoice.setting";

export class CustomerRequest {
  readonly address?: CustomerAddress;
  readonly description?: string;
  readonly email?: string;
  readonly metadata?: any;
  readonly name?: string;
  readonly phone?: string;
  readonly shipping?: ShippingAddress;
  readonly balance?: number;
  readonly coupon?: number;
  readonly default_source?: string;
  readonly invoice_prefix?: string;
  readonly invoice_settings?: InvoiceSetting;
  readonly next_invoice_sequence?: number;
  readonly preferred_locales?: number;
  readonly promotion_code?: number;
  readonly source?: string;
  readonly tax?: Tax;
  readonly tax_exempt?: string;
}
