import { Module } from '@nestjs/common';
import { CardService } from './card.service';
import { CardController } from './card.controller';
import { UserRepository } from '../../users/user.repository';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [CardController],
  exports: [CardService],
  providers: [CardService],
  imports: [TypeOrmModule.forFeature([UserRepository])],
})
export class CardModule {}
