import { ConflictException, HttpException, Injectable } from '@nestjs/common';
import { Stripe } from 'stripe';
import { ApplicationConfiguration } from '../../../shared/services/application.configuration';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '../../users/user.repository';
import { CommonUtils } from '../../../shared/services/common.utils';
import { CommonMessages } from '../../../common/constants/common.messages';
import { Severity } from '../../../exception/severity';
import { ResourceContent } from '../../../exception/resource.content';
import { I18nService } from 'nestjs-i18n';
import { BusinessException } from '../../../exception/business.exception';
import { PaymentIntent } from './dto/payment.intent';
import { UserNonPii } from '../../users/user.non.pii';
import { WrapperLoggerFactory } from '../../../logging/wrapper.logger.factory';
import { CardRequestDto } from './dto/card.request.dto';

@Injectable()
export class CardService {
  private stripe: Stripe;
  private readonly logger = WrapperLoggerFactory.getInstance(CardService.name);
  constructor(
    private applicationConfig: ApplicationConfiguration,
    @InjectRepository(UserRepository)
    private userNonPiiRepository: UserRepository,
    private i18nService: I18nService,
  ) {
    this.stripe = applicationConfig.stripe;
  }

  async getAllCards(
    userId: string,
    limit: number,
    endingBefore?: string,
    startingAfter?: string,
  ) {
    const user = await this.getUserByUserId(userId);
    this.logger.debug(`This is users ${user}`);
    this.logger.log(`This is size ${limit}`);
    return this.stripe.customers.listSources(user.stripeCustomerId, {
      object: 'card',
      limit: limit,
      starting_after: startingAfter,
      ending_before: endingBefore,
    });
  }

  async setupIntent(userId: string, stripeTokenId: string) {
    const user = await this.getUserByUserId(userId);
    const card = await this.getCardFromToken(
      user.stripeCustomerId,
      stripeTokenId,
    );
    let paymentMethodId;
    if (!card) {
      const customerSource = await this.stripe.customers.createSource(
        user.stripeCustomerId,
        {
          source: stripeTokenId,
        },
      );
      paymentMethodId = customerSource.id;
    } else {
      paymentMethodId = card.id;
    }
    return this.stripe.setupIntents.create({
      customer: user.stripeCustomerId,
      confirm: true,
      description: `Setup Intent for user ${user.email}`,
      payment_method: paymentMethodId,
      payment_method_types: ['card'],
    });
  }

  async createPaymentIntent(userId: string, paymentIntent: PaymentIntent) {
    const user = await this.getUserByUserId(userId);
    return this.stripe.paymentIntents.create({
      customer: user.stripeCustomerId,
      amount: paymentIntent.amount,
      confirm: paymentIntent.confirm,
      currency: paymentIntent.currency,
      payment_method: paymentIntent.paymentMethod,
      payment_method_types: ['card'],
    });
  }

  async getUserByUserId(userId: string): Promise<UserNonPii> {
    const user = await this.userNonPiiRepository.findOne(userId, {
      where: {
        isDeleted: false,
      },
    });
    if (!user) {
      const resourceContent = new ResourceContent();
      resourceContent.issues.push(
        await CommonUtils.populateIssue(
          this.i18nService,
          CommonMessages.NOT_FOUND,
          Severity.ERROR,
          {
            entityName: 'user',
            field: 'userId',
            value: userId,
          },
        ),
      );
      throw new BusinessException(resourceContent);
    }
    return user;
  }

  async updateDefaultPaymentMethod(
    userId: string,
    cardRequest: CardRequestDto,
  ): Promise<Stripe.Customer> {
    const user = await this.getUserByUserId(userId);
    return await this.stripe.customers.update(user.stripeCustomerId, {
      source: cardRequest.source,
      metadata: cardRequest.metadata,
    });
  }

  async createCard(
    userId: string,
    cardRequest: CardRequestDto,
  ): Promise<Stripe.CustomerSource> {
    const user = await this.getUserByUserId(userId);
    const card = await this.getCardFromToken(
      user.stripeCustomerId,
      cardRequest.source,
    );
    if (card) {
      throw new ConflictException(`CardId ${card.id} already in use`);
    }
    return this.stripe.customers.createSource(user.stripeCustomerId, {
      source: cardRequest.source,
      metadata: cardRequest.metadata,
    });
  }

  async getCardFromToken(customerId: string, stripeTokenId: string) {
    try {
      const token = await this.stripe.tokens.retrieve(stripeTokenId);
      return await this.stripe.customers.retrieveSource(
        customerId,
        token.card.id,
      );
    } catch (err) {
      return null;
    }
  }
}
