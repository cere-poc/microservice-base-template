import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CardService } from './card.service';
import { PaymentIntent } from './dto/payment.intent';
import { CardRequestDto } from './dto/card.request.dto';

@Controller('/api/v1')
@ApiTags('Stripe')
export class CardController {
  constructor(private cardService: CardService) {}
  @Get('/users/:userId/cards')
  getAllCards(
    @Param('userId') customerId: string,
    @Query('size') size = 10,
    @Query('endingBefore') endingBefore,
    @Query('startingAfter') startingAfter,
  ): Promise<any> {
    return this.cardService.getAllCards(
      customerId,
      size,
      endingBefore,
      startingAfter,
    );
  }

  @Post('/users/:userId/setup-intent')
  @HttpCode(HttpStatus.CREATED)
  createSetupIntent(
    @Param('userId') userId: string,
    @Query('stripeTokenId') stripeTokenId: string,
  ) {
    return this.cardService.setupIntent(userId, stripeTokenId);
  }

  @Post('/users/:userId/payment-intent')
  @HttpCode(HttpStatus.CREATED)
  createPaymentIntent(
    @Param('userId') userId: string,
    @Body() paymentIntent: PaymentIntent,
  ) {
    return this.cardService.createPaymentIntent(userId, paymentIntent);
  }

  @Post('/users/:userId/cards')
  @HttpCode(HttpStatus.CREATED)
  createCard(
    @Param('userId') userId: string,
    @Body() cardRequest: CardRequestDto,
  ) {
    return this.cardService.createCard(userId, cardRequest);
  }

  @Put('/users/:userId/customers')
  updateDefaultPaymentMethod(
    @Param('userId') userId: string,
    @Body() cardRequest: CardRequestDto,
  ) {
    return this.cardService.updateDefaultPaymentMethod(userId, cardRequest);
  }
}
