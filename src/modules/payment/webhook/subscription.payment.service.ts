import { SubscriptionPayment } from './subscription.payment';
import { SubscriptionPaymentRepository } from './subscription.payment.repository';
import { UserRepository } from '../../users/user.repository';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class SubscriptionPaymentService {
  constructor(
    @InjectRepository(SubscriptionPaymentRepository)
    private subscriptionPaymentRepository: SubscriptionPaymentRepository,
    @InjectRepository(UserRepository)
    private userNonPiiRepository: UserRepository,
  ) {}
  async createEventLog(event) {
    const subscriptionPayment = new SubscriptionPayment();
    subscriptionPayment.eventType = event.type;
    const object = event.data.object;
    if (!object.customer) {
      return Promise.resolve(null);
    }
    subscriptionPayment.status = object.status
      ? object.status.toUpperCase()
      : 'UNKNOWN';
    subscriptionPayment.stripeEventId = event.id;
    subscriptionPayment.stripeObjectId = object.id;
    subscriptionPayment.userNonPii = await this.userNonPiiRepository.findOne({
      where: {
        stripeCustomerId: object.customer,
      },
    });
    await this.subscriptionPaymentRepository.manager.transaction(async (em) => {
      await em.save(subscriptionPayment);
    });
    return Promise.resolve(1);
  }
}
