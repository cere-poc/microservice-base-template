import { EntityRepository, Repository } from 'typeorm';
import { SubscriptionPayment } from './subscription.payment';
import { UserNonPii } from '../../users/user.non.pii';
import { Injectable } from '@nestjs/common';

@EntityRepository(UserNonPii)
@Injectable()
export class SubscriptionPaymentRepository extends Repository<SubscriptionPayment> {}
