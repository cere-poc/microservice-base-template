import { Controller, HttpStatus, Post, Req, Res } from '@nestjs/common';
import { Response, Request } from 'express';
import { SubscriptionPaymentService } from './subscription.payment.service';

@Controller('api/v1')
export class WebHookController {
  constructor(private subscriptionPaymentService: SubscriptionPaymentService) {}
  @Post('/webhook')
  async subscriber(
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ) {
    const event = req.body;

    if (!event.type) {
      res.status(HttpStatus.NO_CONTENT);
    } else {
      const result = await this.subscriptionPaymentService.createEventLog(
        event,
      );
      res.status(result ? HttpStatus.OK : HttpStatus.NO_CONTENT);
    }
  }
}
