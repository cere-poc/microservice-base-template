import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from '../../users/user.repository';
import { SubscriptionPaymentRepository } from './subscription.payment.repository';
import { WebHookController } from './web.hook.controller';
import { SubscriptionPaymentService } from './subscription.payment.service';
import { UserModule } from '../../users/user.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserRepository, SubscriptionPaymentRepository]),
    UserModule,
  ],
  controllers: [WebHookController],
  providers: [SubscriptionPaymentService, SubscriptionPaymentRepository],
})
export class WebHookModule {}
