import { AbstractEntity } from '../../../common/abstract.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { UserNonPii } from '../../users/user.non.pii';
@Entity({
  name: 'subscription_payment',
})
export class SubscriptionPayment extends AbstractEntity {
  @Column({
    name: 'subscription_payment_id',
    type: 'uuid',
    primary: true,
    generated: 'uuid',
  })
  subscriptionPaymentId: string;

  @Column({
    name: 'status',
    type: 'varchar',
    nullable: true,
  })
  status: string;
  @Column({
    name: 'event_type',
    type: 'varchar',
    nullable: true,
  })
  eventType: string;
  @Column({
    name: 'stripe_event_id',
    type: 'varchar',
    nullable: true,
  })
  stripeEventId: string;
  @Column({
    name: 'stripe_object_id',
    type: 'varchar',
    nullable: true,
  })
  stripeObjectId: string;

  @ManyToOne(
    () => UserNonPii,
    (userNonPii) => userNonPii.subscriptionPayments,
    {
      lazy: true,
      nullable: true,
    },
  )
  @JoinColumn({
    name: 'user_id',
  })
  userNonPii: UserNonPii;
}
