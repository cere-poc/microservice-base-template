import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from '../../users/user.repository';
import { SubscriptionService } from './subscription.service';
import { SubscriptionController } from './subscription.controller';
import { CardModule } from '../card/card.module';

@Module({
  controllers: [SubscriptionController],
  exports: [SubscriptionService],
  providers: [SubscriptionService],
  imports: [TypeOrmModule.forFeature([UserRepository]), CardModule],
})
export class SubscriptionModule {}
