import {
  Controller,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { SubscriptionService } from './subscription.service';
import { ApiCreatedResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Stripe } from 'stripe';
import Subscription from 'stripe';

@Controller('api/v1')
@ApiTags('Stripe')
export class SubscriptionController {
  constructor(private subscriptionService: SubscriptionService) {}
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: Subscription,
  })
  @Post('/users/:userId/prices/:priceId/subscriptions')
  async createSubscription(
    @Param('userId') userId: string,
    @Param('priceId') priceId: string,
    @Query('couponId') couponId: string,
  ): Promise<Stripe.Subscription> {
    return await this.subscriptionService.createSubscription(
      userId,
      priceId,
      couponId,
    );
  }
}
