import { Stripe } from 'stripe';
import { ApplicationConfiguration } from '../../../shared/services/application.configuration';
import { CardService } from '../card/card.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class SubscriptionService {
  private stripe: Stripe;
  constructor(
    private applicationConfig: ApplicationConfiguration,
    private cardService: CardService,
  ) {
    this.stripe = applicationConfig.stripe;
  }

  async createSubscription(
    userId: string,
    priceId: string,
    couponId: string,
  ): Promise<Stripe.Subscription> {
    const user = await this.cardService.getUserByUserId(userId);
    return await this.stripe.subscriptions.create({
      customer: user.stripeCustomerId,
      items: [
        {
          price: priceId,
        },
      ],
      coupon: couponId,
    });
  }
}
