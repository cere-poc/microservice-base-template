import {
  HealthCheck,
  HealthCheckResult,
  HealthCheckService,
  MicroserviceHealthIndicator,
  TypeOrmHealthIndicator,
} from '@nestjs/terminus';
import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { KafkaOptions, RedisOptions, Transport } from '@nestjs/microservices';
import { ApplicationConfiguration } from '../../shared/services/application.configuration';

@Controller('/health')
export class HealthController {
  constructor(
    private healthCheckService: HealthCheckService,
    private microserviceHealthIndicator: MicroserviceHealthIndicator,
    private dbHealthIndicator: TypeOrmHealthIndicator,
    private applicationConfiguration: ApplicationConfiguration,
  ) {}

  @Get('/liveness')
  @ApiTags('Health')
  @HealthCheck()
  liveNessHealth(): Promise<HealthCheckResult> {
    return this.globalHealth();
  }

  @Get('/readiness')
  @ApiTags('Health')
  @HealthCheck()
  readinessHealth(): Promise<HealthCheckResult> {
    return this.healthCheckService.check([
      async () =>
        this.microserviceHealthIndicator.pingCheck<KafkaOptions>('kafka', {
          transport: Transport.KAFKA,
          options: {
            client: {
              brokers: this.applicationConfiguration.getArrayString(
                'KAFKA_BROKER',
                ',',
              ),
            },
          },
          timeout: 50000,
        }),
    ]);
  }
  @Get()
  @ApiTags('Health')
  @HealthCheck()
  health() {
    return this.globalHealth();
  }

  private globalHealth(): Promise<HealthCheckResult> {
    return this.healthCheckService.check([
      async () =>
        this.dbHealthIndicator.pingCheck('postgres', {
          timeout: 5000,
        }),
    ]);
  }
}
