import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health.controller';
import { HealthIndicatorImpl } from './health.indicator.impl';

@Module({
  imports: [TerminusModule],
  controllers: [HealthController],
  providers: [HealthIndicatorImpl],
})
export class HealthCheckerModule {}
