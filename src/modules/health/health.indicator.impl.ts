import { HealthIndicator } from '@nestjs/terminus';
import { Injectable } from '@nestjs/common';
import { getManager } from 'typeorm';
@Injectable()
export class HealthIndicatorImpl extends HealthIndicator {
  async health() {
    try {
      const version = await getManager().query('SELECT VERSION()');
      console.log(version);
      return this.getStatus('postgresql', true);
    } catch (e) {
      return this.getStatus('postgresql', false);
    }
  }
}
