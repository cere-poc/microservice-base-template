import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';

import { diskStorage } from 'multer';
import { multerOption } from './upload/configUpload';
import { uploadFileAWS } from './upload/upload-service';

@Controller('files')
export class FilesController {
  @Post('/upload-files')
  @UseInterceptors(
    FilesInterceptor('files', multerOption.maximumFile, {
      storage: diskStorage({
        destination: multerOption.defaultDest,
        filename: multerOption.editFileName,
      }),
      fileFilter: multerOption.fileFilter,
    }),
  )
  uploadFiles(
    @UploadedFiles() files: Array<Express.Multer.File>,
  ): Array<Express.Multer.File> {
    console.log(files);
    return files;
  }

  @Post('/upload-aws')
  @UseInterceptors(
    FilesInterceptor('files', multerOption.maximumFile, {
      storage: diskStorage({
        filename: multerOption.editFileName,
      }),
      fileFilter: multerOption.fileFilter,
    }),
  )
  uploadFilesAws(
    @UploadedFiles() files: Array<Express.Multer.File>,
  ): Promise<string> {
    const res = uploadFileAWS(files);
    return res;
  }
}
