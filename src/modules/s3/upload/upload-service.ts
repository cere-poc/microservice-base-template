import * as AWS from 'aws-sdk';
import { s3Config } from './configUpload';

export const uploadFileAWS = async (
  files: Array<Express.Multer.File>,
): Promise<string> => {
  const s3 = new AWS.S3(s3Config);

  const promises = files.map((file) => {
    const params = {
      Bucket: 'cerebral-file-upload-test',
      Key: file.originalname,
      Body: file.filename,
    };
    s3.upload(params).promise();
  });

  const res = await Promise.all(promises);
  console.log('res s3 ', res);

  return 'success';
};

export const checkExtensionFile = (req: any, file: any, cb: any) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return cb(new Error('Not support type file upload ...!'), false);
  }
  return cb(null, true);
};

export const editFileName = (req: any, file: any, cb: any) => {
  return cb(null, file.originalname);
};
