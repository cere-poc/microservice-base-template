import {
  ArgumentMetadata,
  HttpStatus,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';
import { I18nService } from 'nestjs-i18n';
import { ResourceContent } from '../exception/resource.content';
import { Issue } from '../exception/issue';
import { COMMON_CODES, COMMON_MESSAGES } from '../common/constants/constants';
import { CommonMessages } from '../common/constants/common.messages';
import { ConstraintConstants } from '../common/constants/constraint.constants';
import { Severity } from '../exception/severity';
import { BusinessException } from '../exception/business.exception';

@Injectable()
export class CustomValidationPipe implements PipeTransform<any> {
  constructor(private i18nService: I18nService) {}
  async transform(value: any, metadata: ArgumentMetadata): Promise<any> {
    if (!metadata.metatype) {
      return value;
    }
    const obj = plainToClass(metadata.metatype, value);
    const errors = await validate(obj, {
      enableDebugMessages: true,
    });
    if (errors.length > 0) {
      const resourceContent = new ResourceContent();
      for (const err of errors) {
        resourceContent.issues.push(await this.populateIssue(err));
      }
      resourceContent.status = HttpStatus.BAD_REQUEST;
      throw new BusinessException(resourceContent);
    }
    return value;
  }

  private async populateIssue(err: ValidationError): Promise<Issue> {
    const keys = Object.keys(err.constraints);
    let key = CommonMessages.INVALID;
    const issue = new Issue();
    issue.severity = Severity.ERROR;
    const fieldName = err.property;
    const args = { field: fieldName };
    args.field = fieldName;
    if (keys.length > 0) {
      key = keys[0];
      switch (key) {
        case ConstraintConstants.NOT_EMPTY:
          key = CommonMessages.REQUIRED;
          break;
        case ConstraintConstants.MIN_LENGTH:
          Object.assign(args, { value: err.contexts[key].value });
          key = CommonMessages.MINLENGTH;
          break;
        case ConstraintConstants.EMAIL:
          key = CommonMessages.EMAIL;
          break;
        case ConstraintConstants.LOCAL_DATE:
          key = CommonMessages.INVALID;
        default:
          key = CommonMessages.INVALID;
          break;
      }
    }
    [issue.code, issue.details] = await Promise.all([
      this.getCode(key),
      this.getMsg(key, args),
    ]);
    return issue;
  }

  async getCode(code: string): Promise<string> {
    return await this.i18nService.translate(COMMON_CODES.concat(code), {
      lang: 'en',
    });
  }
  async getMsg(
    code: string,
    args:
      | (
          | {
              [k: string]: any;
            }
          | string
        )[]
      | {
          [k: string]: any;
        },
  ): Promise<string> {
    return await this.i18nService.translate(COMMON_MESSAGES.concat(code), {
      lang: 'en',
      args: args,
    });
  }
}
