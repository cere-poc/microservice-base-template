import { ArgumentMetadata, PipeTransform } from '@nestjs/common';
import { PageRequest } from '../common/dto/page.request';
import { SortGenerator } from '../shared/services/sort.generator';

export class SortPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata): any {
    const { type } = metadata;
    if (type === 'query' && value && value.page) {
      return this.transformToPageable(value);
    }
    return value;
  }

  transformToPageable(value: any) {
    const pageable = new PageRequest();
    pageable.page = value.page;
    pageable.size = value.size;
    let sorts = value.sort;
    if (!Array.isArray(sorts)) {
      sorts = Array.of(sorts);
    }
    return pageable;
  }
}
