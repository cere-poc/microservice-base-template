// import { TypeOrmModuleOptions } from '@nestjs/typeorm';
// import { UserNonPii } from 'src/modules/users/user.non.pii';
// import { SubscriptionPayment } from 'src/modules/payment/webhook/subscription.payment';

const getBoolean = (value: String):boolean => value === 'true';

export const baseConfig = () => {
  const { REDIS_HOST, REDIS_PORT, REDIS_TTL,
    FALLBACK_LANGUAGE, ENABLE_WATCH, DB_SYNC } = process.env;
  return {
    REDIS_HOST: REDIS_HOST.toLowerCase() || 'localhost',
    REDIS_PORT: parseInt(REDIS_PORT) || 6379,
    REDIS_TTL: parseInt(REDIS_TTL) || 2000,
    FALLBACK_LANGUAGE: FALLBACK_LANGUAGE.toLowerCase() || 'en',
    ENABLE_WATCH: getBoolean(ENABLE_WATCH),
    DB_SYNC: getBoolean(DB_SYNC),
  }
};

// export const typeOrmConfig = (): TypeOrmModuleOptions => {
//   const entities = [UserNonPii, SubscriptionPayment];
//   const { REDIS_HOST, REDIS_PORT, REDIS_TTL,
//     DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DB_DATABASE,
//     ENABLE_ORMLOGS, FALLBACK_LANGUAGE, ENABLE_WATCH } = process.env;

//   const migrations = [__dirname + '/../../migrations/*{.ts,.js}'];
//   return {
//     entities,
//     migrations,
//     keepConnectionAlive: true,
//     type: 'postgres',
//     host: DB_HOST,
//     port: parseInt(DB_PORT),
//     username: DB_USERNAME,
//     password: DB_PASSWORD,
//     database: DB_DATABASE,
//     //subscribers: [UserSubscriber],
//     migrationsRun: true,
//     synchronize: true,
//     logger: 'advanced-console',
//     logging: getBoolean(ENABLE_ORMLOGS),
//   };
// }
