import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class createArticleTable1628924518162 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'articles',
            columns: [
              {
                name: 'id',
                type: 'int',
                isPrimary: true,
                isGenerated: true,
                generationStrategy: 'increment',
              },
              {
                name: 'title',
                type: 'varchar',
                isUnique: true,
              },
              {
                name: 'content',
                type: 'varchar',
              },
              {
                name: 'isPublic',
                type: 'smallint',
                default: 1,
              },
              {
                name: 'createdAt',
                type: 'timestamp',
                default: 'now()',
                isNullable: true,
              },
              {
                name: 'updatedAt',
                type: 'timestamp',
                default: 'now()',
                isNullable: true,
              },
            ],
          }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('articles')
    }

}
