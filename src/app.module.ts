import {
  ClassSerializerInterceptor,
  MiddlewareConsumer,
  Module,
  NestModule,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HealthCheckerModule } from './modules/health/health.check.module';
import { UserModule } from './modules/users/user.module';
import { SharedModule } from './shared/shared.module';
// import { ApplicationConfiguration } from './shared/services/application.configuration';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { LoggingInterceptor } from './interceptors/logging.interceptor';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';
import { GlobalExceptionHandler } from './filters/global.exception.handler';
import { I18nJsonParser, I18nModule } from 'nestjs-i18n';
import * as path from 'path';
import { AutomapperModule } from '@automapper/nestjs';
import { classes } from '@automapper/classes';
import { IntegrationModule } from './integration/integration.module';
import { UserClientModule } from './modules/client/user.client.module';
import { CacheInterceptor } from '@nestjs/common/cache';
import { CacheModule } from '@nestjs/common';
import * as redisStore from 'cache-manager-redis-store';
// import { RedisInfo } from './integration/redis/constant';
import { CardModule } from './modules/payment/card/card.module';
import { SubscriptionModule } from './modules/payment/subcription/subscription.module';
import { WebHookModule } from './modules/payment/webhook/web.hook.module';
import { ConfigService } from '@nestjs/config';
import { baseConfig } from 'src/config/configuration'
// const ENV = process.env.NODE_ENV;

import { UserNonPii } from 'src/modules/users/user.non.pii';
import { SubscriptionPayment } from 'src/modules/payment/webhook/subscription.payment';

const ormModuleConfig = TypeOrmModule.forRootAsync({
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    return {
      type: configService.get('DB_TYPE'),
      host: configService.get('DB_HOST'),
      port: configService.get('DB_PORT'),
      username: configService.get('DB_USERNAME'),
      password: configService.get('DB_PASSWORD'),
      database: configService.get('DB_DATABASE'),
      entities: [UserNonPii, SubscriptionPayment],
      logger: 'advanced-console',
      logging: configService.get('ENABLE_ORMLOGS'),
    } as TypeOrmModuleAsyncOptions;
  },
})

@Module({
  imports: [
    ConfigModule.forRoot({
      // envFilePath: [!ENV ? '.env' : `.env.${ENV}`],
      load: [baseConfig],
      isGlobal: true,
    }),
    CacheModule.registerAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        store: redisStore,
        host: configService.get<string>('REDIS_HOST'),
        port: configService.get<number>('REDIS_PORT'),
        ttl: configService.get<number>('REDIS_TTL'),
      }),
    }),
    ormModuleConfig,
    I18nModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        fallbackLanguage: configService.get<string>('FALLBACK_LANGUAGE'),
        parserOptions: {
          path: path.join(__dirname, '/i18n/'),
          watch: configService.get<boolean>('ENABLE_WATCH'),
        },
      }),
      imports: [SharedModule],
      parser: I18nJsonParser,
      inject: [ConfigService],
    }),
    AutomapperModule.forRoot({
      options: [
        {
          name: 'autoMapper',
          pluginInitializer: classes,
        },
      ],
      singular: true,
    }),
    IntegrationModule,
    HealthCheckerModule,
    UserModule,
    UserClientModule,
    CardModule,
    SubscriptionModule,
    WebHookModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: GlobalExceptionHandler,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: CacheInterceptor,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {}
}
