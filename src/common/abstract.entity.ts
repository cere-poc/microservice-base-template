import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

export abstract class AbstractEntity extends BaseEntity {
  @Column({
    name: 'created_by',
    type: 'varchar',
    nullable: true,
  })
  createdBy: string;
  @Column({
    name: 'updated_by',
    type: 'varchar',
    nullable: true,
  })
  updatedBy: string;
  @CreateDateColumn({
    name: 'created_on',
    type: 'timestamptz',
  })
  createdOn: Date;
  @UpdateDateColumn({
    name: 'updated_on',
    type: 'timestamptz',
  })
  updatedOn: Date;

  @BeforeInsert()
  protected beforeInsert(): void {
    this.createdBy = '9a1f578e-1c10-493d-abed-84794ba59303';
    this.updatedBy = '9a1f578e-1c10-493d-abed-84794ba59303';
  }

  @BeforeUpdate()
  protected beforeUpdate(): void {
    this.updatedBy = '9a1f578e-1c10-493d-abed-84794ba59303';
  }
}
