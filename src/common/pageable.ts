export interface Pageable {
  getPageNumber(): number;
  getPageSize(): number;
  getOffset(): number;
  next(): Pageable;
  previousOrFirst(): Pageable;
  first(): Pageable;
  hasPrevious(): boolean;
}
