export class NavigationInfo {
  first: string;
  previous: string;
  next: string;
  last: string;
}
