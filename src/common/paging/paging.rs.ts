import { NavigationInfo } from './navigation.info';

export class PagingRs<T> {
  totalCount: number;
  data: T[];
  paging: NavigationInfo;
}
