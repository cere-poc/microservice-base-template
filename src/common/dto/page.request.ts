import { Sort } from '../sort/sort';
import { IsInt, IsOptional, Min } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class PageRequest {
  @ApiPropertyOptional({
    minimum: 1,
    maximum: 2147483647,
    title: 'Size',
    exclusiveMaximum: true,
    exclusiveMinimum: true,
    format: 'int32',
    default: 1,
  })
  @Type(() => Number)
  @IsInt()
  @Min(1)
  @IsOptional()
  size: number;
  @ApiPropertyOptional({
    minimum: 1,
    maximum: 2147483647,
    title: 'Page',
    exclusiveMaximum: true,
    exclusiveMinimum: true,
    format: 'int32',
    default: 1,
  })
  @Type(() => Number)
  @IsInt()
  @Min(10)
  @IsOptional()
  page: number;
  sort: Sort;

  getOffset(): number {
    return (this.page - 1) * this.size;
  }
  getPageSize(): number {
    return this.size;
  }
  getPage(): number {
    return this.page;
  }
  public getSort(): Sort {
    return this.sort;
  }
}
