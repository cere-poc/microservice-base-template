export class CommonMessages {
  public static readonly SERVICE_AUTH_REQUIRED: string = 'ERR_401';
  public static readonly INTERNAL_SERVER_ERROR: string = 'ERR_500';
  public static readonly INVALID: string = 'ERR_0811';
  public static readonly REQUIRED: string = 'ERR_0801';
  public static readonly EMAIL: string = 'ERR_0814';
  /**
   * Min length validation message. Format: {0} is required to be at least {1} characters.
   */
  public static readonly MINLENGTH: string = 'ERR_0802';

  public static readonly ENTITY_WITH_DETAIL_NOTFOUND: string = 'ERR_0823';

  /**
   * Error message for Entity already exists
   */
  public static readonly ENTITY_EXISTS: string = 'ERR_0820';
  public static readonly NOT_FOUND: string = 'ERR_0823';
}
