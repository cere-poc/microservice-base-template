export class ConstraintConstants {
  public static readonly NOT_EMPTY = 'isNotEmpty';
  public static readonly EMAIL = 'isEmail';
  public static readonly LOCAL_DATE = 'isLocalDate';
  public static readonly MIN_LENGTH = 'minLength';
}
