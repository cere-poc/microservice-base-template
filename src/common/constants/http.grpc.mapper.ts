import { HttpStatus } from '@nestjs/common';
import { Status } from '@grpc/grpc-js/build/src/constants';

export const errorCodeToGrpc = function (status: HttpStatus): Status {
  return Status.INVALID_ARGUMENT;
};
