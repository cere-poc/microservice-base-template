import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';

const validator = require('validator');

export enum Direction {
  DESC = 'DESC',
  ASC = 'ASC',
}

export class Order {
  @ApiPropertyOptional({
    enum: Direction,
    default: Direction.ASC,
  })
  @IsEnum(Direction)
  @IsOptional()
  direction: Direction;
  @IsOptional()
  @ApiPropertyOptional({
    required: false,
    description: 'property for sort',
  })
  property: string;

  constructor(direction: Direction, property: string) {
    if (validator.isEmpty(property)) {
      throw Error('Property must not null or empty!');
    }
    this.direction = direction ? direction : Sort.DEFAULT_DIRECTION;
    this.property = property;
  }
}

export class Sort {
  public static readonly DEFAULT_DIRECTION: Direction = Direction.ASC;
  orders: Order[];
  constructor(orders: Order[]) {
    this.orders = orders;
  }
}
