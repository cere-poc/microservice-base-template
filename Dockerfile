FROM node:14-alpine

WORKDIR /app

COPY . .

RUN yarn install

EXPOSE 3000

VOLUME [ "/app/.env" ]

ENTRYPOINT ["yarn", "start:prod"]
